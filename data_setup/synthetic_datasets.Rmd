---
title: "Synthetic Datasets"
author: "Russ Hyde"
output:
  pdf_document: default
  html_notebook: default
---

```{r}
library(here)
library(ggplot2)
library(magrittr)
library(readr)
library(tidyr)
```

# Synthetic gene-expression dataset

A synthetic dataset for use in making boxplots. We want to be able to plot a few
treatments on the x-axis, a box-plot summary of some measurable outcome on the
y-axis, and overlay jittered or ordered 'raw' data on top of the box-plot.

The data are an imaginary gene-expression dataset corresponding to the treatment
of several patients with a couple of drugs. We construct them using a
hierarchical model. For a given gene, there is a patient-specific baseline of
gene expression upon which drug-treatments have an influence on gene expression.
The data would need log-transforming before plotting.


```{r}
get_boxplot_dataset <- function(rng.seed = 0){
  set.seed(rng.seed)
  patients <- factor(paste0("P", 1:5))
  genes    <- paste0("G", 1:10)
  drugs    <- factor(c("CTRL", "D1", "D2"))
  
  g_matrix <- sapply(genes, function(g){
    # Baseline for the expression of the current gene
    g_ave <- rnorm(1)
    # The baseline for the different patients is randomly distributed around
    #   the gene-specific baseline
    p_baseline <- rep(rnorm(length(patients), g_ave, sd = 0.2),
                      times = length(drugs))
    # The effect of a given drug on gene expression
    #  - control treatment has no effect
    #  - drugs D1, D2, ... have a randomly sampled effect, but for a given drug
    #    this effect is constant across the patients
    d_effect <- rep(c(0, rnorm(length(drugs) - 1, mean = 0, sd = 0.5)),
                      each = length(patients))
    # log2 of gene expression is an additive combination of patient-baseline
    #   and drug-effect, plus a bit of noise
    log_g      <- rnorm(length(p_baseline),
                        mean = p_baseline + d_effect,
                        sd = 0.1)
    # return the gene expression values in multiplicative space
    2 ^ log_g
  })
  data.frame(
    expand.grid(patient = patients, drug = drugs),
    g_matrix
  )
}
```

```{r}
genes <- get_boxplot_dataset()
```

```{r}
readr::write_tsv(genes, here("data", "boxplot_genes.tsv"))
```

```{r}
genes
```

```{r}
genes %>%
  ggplot(aes(x = drug, y = G1, fill = drug)) +
  geom_boxplot()
```

```{r}
genes %>%
  tidyr::gather("gene", "intensity", 3:12) %>%
  dplyr::filter(gene %in% c("G1", "G2", "G3", "G4")) %>%
  dplyr::mutate(log_intensity = log2(intensity)) %>%
  ggplot(aes(x = drug, y = log_intensity)) +
  geom_boxplot() +
  facet_wrap(~ gene, nrow = 2)
```