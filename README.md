# README #

Scripts for setting up toy datasets, and documents outlining challenges for use
in POG-code-club (an introductory R coding club).

## What is this repository for? ##

* R coding challenges, mainly visualisation challenges in ggplot2 as of Oct
2017

## How do I get set up? ##

* Just copy a file from the challenges/ dir onto your computer and complete the
challenges in the \{r\} blocks

## Contribution guidelines ##

* New challenge scripts to be added as self-contained R-markdown documents

## Who do I talk to? ##

* @russH: russell.hyde AT glasgow
* Other community or team contact
