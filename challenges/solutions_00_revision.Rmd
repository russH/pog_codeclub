---
title: "Solutions for Code-Club-0"
author: "Russ Hyde, University of Glasgow"
date: "`r format(Sys.time(), '%Y-%m-%d')`"
output:
  html_document:
    df_print: paged
---

```{r solution_1, eval=FALSE, echo=FALSE}
# Challenge 1 Solution:
ggplot(data = tidy_hills, aes(x = dist, y = time)) +
  geom_point() +
  xlim(0, NA) +
  ylim(0, NA)
```

```{r solution_2, eval=FALSE, echo=FALSE}
# Challenge 2 Solution:
ggplot(data = tidy_hills, aes(x = climb, y = time)) +
  geom_point() +
  labs(
    x = "Height climbed / feet",
    y = "Record time / min"
    ) +
  xlim(0, NA) +
  ylim(0, NA)
```

```{r solution_3, eval=FALSE, echo=FALSE}
# Challenge 3 Solution:
ggplot(data = tidy_hills, aes(x = dist, y = climb, size = time)) +
  geom_point() +
  labs(
    x = "Race distance / miles",
    y = "Height climbed / feet",
    size = "Record time / min"
    ) +
  lims(
    x = c(0, NA),
    y = c(0, NA)
    )
```
