---
title: "Solutions for Dose-Response-Curve Analysis"
author: "Russ Hyde"
date: "19 April 2018"
output: html_document
---

<!-- This document should not be knitted itself:
    - It's chunks are imported into codeclub_05_ic50.Rmd to provide example solutions
    to the challenges defined therein
    - The solutions depend upon variables that are set-up within codeclub_05_ic50.Rmd,
    hence, here the chunks all have eval=FALSE
    -->

```{r solution_1, eval = FALSE, echo = FALSE}
# Challenge 1 - Solution

concs <- c(0, 0.1, 0.3, 1, 3, 10)

cell_counts <- sample_ec50_dataset(
  concs, min_val = 10, max_val = 90, ec_50 = 0.5, hill = 1, sd = 3
)
```

```{r solution_2, eval = FALSE, echo = FALSE}
# Challenge 2 - Solution

dose_response <- data.frame(
  conc = concs,
  cell_count = cell_counts
)
```

```{r solution_3, eval = FALSE, echo = FALSE}
# Challenge 3 - Solution

p <- ggplot(
  data = dose_response, aes(x = conc, y = cell_count)
) +
  geom_point() +
  geom_line() +
  labs(x = "Drug Concentration", y = "Cell Count") +
  ylim(0, NA)

p
```

```{r solution_4, eval = FALSE, echo = FALSE}
# Challenge 4 - Solution
# - Alternative: p + scale_x_log10()

p + scale_x_continuous(trans = "log10")
```

```{r solution_5, eval = FALSE, echo = FALSE}
# Challenge 5 - Solution

fit <- lm(cell_count ~ conc, data = dose_response)
```

```{r solution_6, eval = FALSE, echo = FALSE}
# Challenge 6 - Solution

linear_counts <- predict(fit, newdata = additional_data)
```

```{r solution_7, eval = FALSE, echo = FALSE}
# Challenge 7 - Solution

summary(curved_fit)
```

```{r solution_8, eval = FALSE, echo = FALSE}
# Challenge 8 - Solution

curved_counts <- predict(curved_fit, newdata = additional_data)
```

```{r solution_9, eval = FALSE, echo = FALSE}
# Challenge 9 - Solution

curved_fitted_data <- data.frame(
  additional_data,
  cell_count = curved_counts
)

ggplot(dose_response, aes(x = conc, y = cell_count)) +
  geom_point() +
  geom_line(data = curved_fitted_data) +
  ylim(0, NA)
```
